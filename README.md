# Docker Tests Workshop

# Author
Ivan Latka, [evee.latkinson@gmail.com](mailto:evee.latkinson@gmail.com)


# Goal
Goal of this workshop is to introduce to the docker and its usage in integration tests between application components.
This repo contains one such example.

Simple app connected to the postgres DB and tests of simple network failures between them via py-docker.

Worshop is mostly intended for docker beginners and intemediate dvelopers.
You should have at least basic experience with python.  

# Content 
First part of the workshop will be spent on setup of this repo, so don't forget to bring laptops.

In the second part you try to adjust few existing tests a discuss various approaches.
Do not hesitate and ask any question if you feel stuck. I will gladly help you :-)


# Requirements
- laptop, preferably with some linux or windows, you may try mac os, but due to my unexperience, I will be probably not able to halp you with some setups
- python 3.6 and some IDE (e.g. Pycharm)
- git (optional)
- docker, preferably installed, if not I will try to help you with installation


# Setup
1. make sure you have installed [python 3.5 or 3.6](https://www.python.org/downloads/)
2. Download this workshop project.
    - (Optional) If you have [git](https://git-scm.com/downloads) installed you can
    use command:
        ```
        git clone https://ivan_latka@bitbucket.org/ivan_latka/docker-tests-workshop.git
        ```
    - or download workshop [directly](https://bitbucket.org/ivan_latka/docker-tests-workshop/downloads/?tab=downloads) 
    and just unzip it somewhere into your PC
3. Open workshop as a projects in your python IDE:
    - You can use for example [Pycharm Community Edition](https://www.jetbrains.com/pycharm/download).
4. In this project create python [virtual environment](https://virtualenv.pypa.io/en/stable/installation/) name `venv`.
    `virtualenv -p python 3.6 venv`
    Activate is and inside it run
    ```
    pip install -r app_requirements.txt -r test_requirements.txt
    ```
5. build app image `docker build -t app .`
6. In the project run tests by command
    ```
    pytest
    ```

## Links
- [docker install](https://docs.docker.com/install/)
- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
- [docker-compose reference](https://docs.docker.com/compose/compose-file/)
- [py-docker](http://docker-py.readthedocs.io/en/stable/)
- [python 3.6 download](https://www.python.org/downloads/)
- [python virtual env](https://virtualenv.pypa.io/en/stable/userguide/)
- [pytest](https://docs.pytest.org/en/latest/)
- [@retry decorator](https://pypi.python.org/pypi/retry/)