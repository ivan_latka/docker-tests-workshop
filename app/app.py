#!/usr/bin/env python3.6

import atexit
import logging
import pprint
import psycopg2

from typing import Optional
from flask import Flask, Response, url_for
from app.config import Config

config = Config()
app = Flask(__name__)
app.config.from_object(config)
app.logger.setLevel(getattr(logging, config.FLASK_LOGGING_LEVEL))
app.logger.info('Using following configuration: \n' + pprint.pformat(config.options_dict))
with app.app_context():
    db: Optional[psycopg2.extensions.connection] = None


@app.route('/')
def index():
    url = url_for('db_version')
    return Response(
        mimetype='text/plain',
        response=f'Welcome to the dumbest db interface!\nYou can see db version in: {url}'
    )


@app.route("/db-version")
def db_version():
    db = psycopg2.connect(**config.PG_DB_CONN)
    cursor = db.cursor()
    cursor.execute("SELECT version();")
    db_version_text = cursor.fetchone()
    db.close()
    return Response(mimetype='text/plain', response=f'Here is your database version:\n{db_version_text}')


def exit_app():
    print('\nExiting app ...')


atexit.register(exit_app)
