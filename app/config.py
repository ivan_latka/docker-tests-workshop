import json
import os
import sys
import typing

from typing import Any, Union


class Config:
    """
    Attributes of this class can be overriden by the environment variables of the same name.
    Attributes with None initial value are mandatory and have to be set as environment variable.
    Environment variables are automatically converted from json into python types.
    Suggested typing is used for assertion of converted values.
    """
    FLASK_LOGGING_LEVEL: str = 'INFO'

    # example: PG_DB_CONN='{"host": "localhost", "database": "postgres", "user": "postgres", "password": "postgres", "port": "5432"}'
    PG_DB_CONN: dict = None

    def __init__(self):
        self.options_dict = self.__get_env_configuration()

    def __get_env_configuration(self) -> dict:
        attr_names = [attr_name for attr_name in dir(self) if not attr_name.startswith('_')]
        empty_attributes = list()
        options_dict = dict()

        for attr_name in attr_names:
            setattr(self, attr_name, self.__convert(
                value=os.environ.get(attr_name, getattr(self, attr_name)),
                attr_name=attr_name
            ))

            options_dict[attr_name] = getattr(self, attr_name)

            if options_dict[attr_name] is None:
                empty_attributes.append(attr_name)

        if empty_attributes:
            raise ValueError('Following mandatory environment variables are missing: ' + ','.join(empty_attributes))
        return options_dict

    @staticmethod
    def __convert(value: Any, attr_name: str) -> Any:
        expected_type = vars(Config)['__annotations__'][attr_name]

        if isinstance(type(expected_type), typing.TypingMeta) and '__args__' in dir(expected_type):
            expected_types = expected_type.__args__
        else:
            expected_types = (expected_type,)

        if value is None:
            return value
        elif isinstance(value, str) and str not in expected_types:
            try:
                converted_value = json.loads(value)
            except json.decoder.JSONDecodeError as error:
                raise type(error)(
                    str(error.args[0]) + f'\nIncorrect config or environment variable "{attr_name}={value}"!'
                ).with_traceback(sys.exc_info()[2])

            assert isinstance(converted_value, expected_types), \
                f'Config attribute "{attr_name}" has incorrect type "{type(converted_value)}", ' \
                f'expected "{expected_types}".'
            return converted_value
        else:
            return value
