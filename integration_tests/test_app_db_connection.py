import requests
import time
from integration_tests.lib.wait import wait_for_docker_log
from retry import retry

from integration_tests.lib.docker_env import DockerEnv


@retry(AssertionError, tries=3, delay=1)
def assertion_db_version():
    assert 'PostgreSQL' in requests.get('http://localhost:5000/db-version').text


def test_normal_app_setup(docker_env: DockerEnv):
    docker_env.run_container(name='db', image='postgres:11.1', ports={'5432/tcp': 5432})
    app_container = docker_env.run_container(
        name='app', image='app', pull=False, ports={'5000/tcp': 5000},
        environment={
            'PG_DB_CONN': '{"host": "db", "database": "postgres", "user": "postgres", "password": "postgres",'
                          ' "port": "5432"}'
        })
    wait_for_docker_log(app_container, 'Using following configuration')
    assertion_db_version()


def test_db_disconnect(docker_env: DockerEnv):
    db_container = docker_env.run_container(name='db', image='postgres:11.1', ports={'5432/tcp': 5432}, pull=False)
    app_container = docker_env.run_container(
        name='app', image='app', pull=False, ports={'5000/tcp': 5000},
        environment={
            'PG_DB_CONN': '{"host": "db", "database": "postgres", "user": "postgres", "password": "postgres",'
                          ' "port": "5432"}'
        })
    wait_for_docker_log(app_container, 'Using following configuration')
    db_container.stop()
    db_container.start()
    wait_for_docker_log(db_container, 'database system is ready to accept connections')
    assertion_db_version()