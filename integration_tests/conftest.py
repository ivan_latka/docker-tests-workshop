import pytest
import logging

from integration_tests.lib.docker_env import DockerEnv

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


@pytest.fixture
def docker_env() -> DockerEnv:
    with DockerEnv() as env:
        yield env
