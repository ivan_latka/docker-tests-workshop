import logging

from retry import retry
from docker.models.containers import Container
from typing import Callable

from integration_tests.lib.assert_docker_logs import AssertDockerLogs


# Gradually waits 1, 1.5, 2.25, 3.375, 5.0625, 7,59375 seconds, until max delay of 10 seconds.
# Timeout in approximately one minute.
# For more details, see documentation of @retry
RETRY_ARGS = {'tries': 10, 'delay': 1, 'backoff': 1.5, 'max_delay': 10, 'logger': None}


@retry(AssertionError, **RETRY_ARGS)
def wait_for_docker_log(container: Container, message: str):
    logging.info(f'Waiting for log message "{message}" from container "{container.name}".')
    with AssertDockerLogs(container) as logs:
        assert message in logs, f'Message "{message}" was not found in container logs!'


@retry(AssertionError, **RETRY_ARGS)
def wait_for_docker_logs(container: Container, assert_func: Callable[[str], None]):
    """
    :param container: Docker container
    :param assert_func: Function should accept str parameter, which contains docker container logs.
        In case of assertion error, function will retry.
    """
    logging.info(f'Waiting for log messages from container "{container.name}".')
    with AssertDockerLogs(container) as logs:
        assert_func(logs)
