import docker


class AssertDockerLogs:
    """
    This context manager access container logs and in case of AssertionError(or any given type),
    it decorates error message with container logs.
    """

    def __init__(self, container: docker.models.containers.Container, error_type: Exception=AssertionError,
                 tail_lines: int=500, log_offset: str=4 * ' '):
        self.container = container
        self.error_type = error_type
        self.tail_lines = tail_lines
        self.log_offset = log_offset

        self.container.reload()
        self.logs = self.container.logs(tail=tail_lines).decode("utf-8")

        if container.status != 'running':
            raise self.__add_container_logs_into_error_msg(RuntimeError(f'Container is not running!'))

    def __enter__(self):
        return self.logs

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type == self.error_type:
            self.__add_container_logs_into_error_msg(exc_val)

    def __add_container_logs_into_error_msg(self, exc_val):
        lines = self.logs.splitlines()
        lines.insert(0, '... start of container log ...')
        lines.append('... end of container log ...')

        if len(exc_val.args) > 0:
            error_msg = str(exc_val.args[0]) + '\n'
        else:
            error_msg = ''

        error_msg += f'Error during log checking of container named "{self.container.name}":\n{self.log_offset}' + \
                     f'\n{self.log_offset}'.join(lines)
        exc_val.args = (error_msg,)
        return exc_val
