import docker
import logging
import requests
import datetime

from docker import DockerClient
from docker.models.containers import Container
from typing import Optional, List


class DockerEnv:
    """
    Context manager for running docker containers.

    **Warning:** Since this class allocates docker resources, you should always make sure that "_destroy" method is
    called at the end. Using this class as context manager will automatically destroy used containers.

    Example:
        with DockerEnv(docker_client=docker_client) as docker_env:
            container = docker_env.run_container(image='hello-world')
    """

    __docker_client: Optional[DockerClient] = None

    def __init__(self, docker_client: Optional[DockerClient]=None,
                 docker_registry_auths: List[dict]=None, pull_images: bool=True, net_name: str='docker_tests'):
        """
        :param docker_client: If none, following method will be used
            http://docker-py.readthedocs.io/en/stable/client.html#docker.client.from_env
        :param docker_registry_auths: Used for authentication into registry during image pull.
            List of dictionaries with following structure:
                [
                    {
                        'url': 'docker_registry_url',

                        'username': 'docker login username',

                        'password': 'docker login password',
                    }
                ]
        :param pull_images: whether to pull docker images before run.
        :param net_name: Name of docker network used among containers.
        """
        DockerEnv.__docker_client = self.docker_client = docker_client or DockerEnv.__docker_client or docker.from_env()
        self.docker_registry_auths = [] if docker_registry_auths is None else docker_registry_auths
        self.pull_images = pull_images
        self.net_name = net_name
        # self.docker_client.networks.list()
        self.network = self.docker_client.networks.create(net_name)
        self.containers = []
        self._ping_docker()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._destroy()

    def _destroy(self):
        """
        Removes all docker resources (containers, volumes) created by this class instance.
        """
        for container in self.containers:
            logging.info(f'Removing container name: "{container.name}", short id: {container.short_id}.')
            try:
                container.remove(v=True, force=True)
            except docker.errors.NotFound:
                logging.warning(f'Skipping removal of container named "{container.name}", because it was not found!')

        self.containers.clear()
        self.network.remove()

    def _ping_docker(self):
        error_message = f'Unable to use docker client with base url "{self.docker_client.api.base_url}"!'
        try:
            assert self.docker_client.ping() is True, error_message
        except requests.exceptions.ConnectionError:
            raise AssertionError(error_message)  # overriding original error, because its traceback is needlessly large

    def _pull_image(self, image: str):
        """
        Pull docker image from the registry and if there is stored authentication for the docker registry, use it.
        """
        if ':' not in image:
            image += ':latest'   # "pull" method will fail if there is no docker tag in the image name

        registry_auth = None

        for auth in self.docker_registry_auths:
            if image.startswith(auth['url']):
                registry_auth = auth
                break

        logging.info(f'Pulling image "{image}".')
        self.docker_client.images.pull(image, auth_config=registry_auth)

    def run_container(self, image: str, name_base: Optional[str]=None, pull: Optional[bool]=None, **kwargs) \
            -> Container:
        """
        Run docker container. As container arguments you can use some already prepared in `self.container_args`.

        :param image: docker image name
        :param name_base: Base for container name, which will be decorated by timestamp.
            Ignored, in case kwargs contain `name`.
        :param pull: Indicates whether to pull the image from the registry.
        :param kwargs: use any arguments mentioned in "run" method


            do not use "detach=true", because it tis invalid input for containers.create()
        :return: docker container
        """
        if name_base and 'name' not in kwargs:
            kwargs['name'] = name_base + datetime.datetime.now().strftime('-%Y-%m-%d--%H-%M-%S')

        if pull is True or (pull is not False and self.pull_images is True):
            self._pull_image(image)

        assert 'image' not in kwargs
        kwargs['image'] = image
        assert 'network' not in kwargs
        kwargs['network'] = self.net_name

        container = self.docker_client.containers.create(**kwargs)
        assert isinstance(container, Container)
        self.containers.append(container)
        logging.info(f'Starting container with attributes: image "{container.image.tags[0]}", name "{container.name}", '
                     f'ID "{container.short_id}"')

        container.start()
        return container