FROM python:3.6-slim

ARG REQUIREMENTS_FILE=app_requirements.txt

ENV APP_PATH=/usr/local/docker-tests-workshop/ \
    FLASK_APP=app/app.py \
    FLASK_RUN_PORT=5000

RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends curl

WORKDIR $APP_PATH

COPY $REQUIREMENTS_FILE ./

RUN pip install pip==18.1 setuptools==40.6.2 -r $REQUIREMENTS_FILE

COPY app/* ./app/

CMD ["flask", "run", "--host", "0.0.0.0"]